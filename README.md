# devops-netology 
first line

Если я правильно понял задание, то описание должно быть в этом файле.

Итак,  в файле terraform/.gitignore  будут игноироваться следующие файлы:
 - сама локальная директория .terraform 
 - файлы с расширением *.tfstate и *.tfstate.*
 - файлы аварийного журнала с расширением *.log
 - файлы, содержащие конфиденциальную информацию с расширением
    *.tfvars и*.tfvars.json
 - override.tf, override.tf.json,*_override.tf,*_override.tf.json
 - конфиг-файлы .terraformrc, terraform.rc
